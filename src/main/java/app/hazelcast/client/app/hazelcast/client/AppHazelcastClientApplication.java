package app.hazelcast.client.app.hazelcast.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppHazelcastClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppHazelcastClientApplication.class, args);
	}

}
