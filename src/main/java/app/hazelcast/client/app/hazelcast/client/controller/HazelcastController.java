package app.hazelcast.client.app.hazelcast.client.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.XmlClientConfigBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;

import app.hazelcast.client.app.hazelcast.client.domain.Message;

@RestController
public class HazelcastController {
	
	private ClientConfig clientConfig = null;
	
	@Autowired
	private HazelcastInstance hz ;
	
	public HazelcastController() {
		try {
			this.clientConfig = new XmlClientConfigBuilder("hazelcast-client.xml").build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		hz = HazelcastClient.newHazelcastClient(clientConfig);
	}

	@GetMapping("/hazelcast-hello-world-client/{name}")
	public String helloWorldClient(@PathVariable String name) {
		IMap<String, String> map = hz.getMap("map");
		try {
			map.put("name", name);
		} catch (Throwable e) {
			e.printStackTrace();
			clientConfig = new ClientConfig();
			System.out.println("Usando valores de conexion por defecto");
			clientConfig.getNetworkConfig().addAddress("172.17.0.3:30010").setSmartRouting(false);
		}
		return "{'status': 'OK'}";
	}
	
	@PostMapping("/hazelcast-put")
	public String messageToHazelcast(@RequestBody Message message) {
		IMap<String, String> map = hz.getMap("map");
		try {
			System.out.println(">>>" + message.toString());
			map.put(message.getWorkflowInstanceKey().toString(), message.toJSON());
		} catch (Throwable e) {
			e.printStackTrace();
			clientConfig = new ClientConfig();
			System.out.println("Usando valores de conexion por defecto");
			clientConfig.getNetworkConfig().addAddress("172.17.0.3:30010").setSmartRouting(false);
		}
		return "{'status': 'OK'}";
	}

}
