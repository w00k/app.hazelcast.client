package app.hazelcast.client.app.hazelcast.client.domain;

public class Message {
	private String message;
	private Long workflowKey;
	private Long workflowInstanceKey;

	public Message() {
	}

	public Message(String message, Long workflowKey, Long workflowInstance) {
		this.message = message;
		this.workflowKey = workflowKey;
		this.workflowInstanceKey = workflowInstanceKey;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getWorkflowKey() {
		return workflowKey;
	}

	public void setWorkflowKey(Long workflowKey) {
		this.workflowKey = workflowKey;
	}

	public Long getWorkflowInstanceKey() {
		return workflowInstanceKey;
	}

	public void setWorkflowInstanceKey(Long workflowInstanceKey) {
		this.workflowInstanceKey = workflowInstanceKey;
	}
	
	public String toString() {
		return "message: " + message + ",  workflowKey:" + workflowKey + ", workflowInstance:" + workflowInstanceKey;
	}
	
	public String toJSON() {
		return "{message: " + message + ",  workflowKey:" + workflowKey + ", workflowInstance:" + workflowInstanceKey + "}";
	}

}
