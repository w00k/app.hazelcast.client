package app.hazelcast.client.app.hazelcast.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.XmlClientConfigBuilder;
import com.hazelcast.core.HazelcastInstance;

import io.zeebe.hazelcast.connect.java.ZeebeHazelcast;

@RestController
public class ZeebeHazelcastController {

	@Autowired
	private HazelcastInstance hz;
	
	private ClientConfig clientConfig = new ClientConfig();
	
	public ZeebeHazelcastController() {
		clientConfig.getNetworkConfig().addAddress("127.0.0.1:5701");
		hz = HazelcastClient.newHazelcastClient(clientConfig);
	}
	
	@GetMapping("/hello-world-client/{name}")
	public String helloWorldClient(@PathVariable String name) {

		try {
			clientConfig = new XmlClientConfigBuilder("hazelcast-client.xml").build();
			System.out.println("Usando configmap");

			ZeebeHazelcast zeebeHazelcast = ZeebeHazelcast.newBuilder(hz)
					.addWorkflowInstanceListener(workflowInstance -> {
						hz.getMap("map").put("name", name);
					}).readFromTail().build();

			zeebeHazelcast.close();

		} catch (Throwable e) {
			e.printStackTrace();
			clientConfig = new ClientConfig();
			System.out.println("Usando valores de conexion por defecto");
			clientConfig.getNetworkConfig().addAddress("172.17.0.3:30010").setSmartRouting(false);
		}
		return "OK";
	}

}
